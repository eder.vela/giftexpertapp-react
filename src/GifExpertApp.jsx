import { useState } from "react";

import { AddCategory } from "./components/AddCategory";
import {GifGrid} from "./components/GifGrid";

export const GifExpertApp = () => {
  
  const [categories, setCategories] = useState(["Dragon Ball"]);

  const onAddCategory = (newCat) => {
    if (categories.includes(newCat)) return
    setCategories([newCat, ...categories])
  }

  return (
    <>
      <h1>GifExpertApp</h1>
      <AddCategory 
        onNewCategory={onAddCategory}
      />
        {
          categories.map( category => 
            <GifGrid 
            category={category} 
            key={category}
            />
            
          )
        }

    </>
  );
};
