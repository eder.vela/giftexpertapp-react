import React, { useEffect, useState } from "react"
import { GifItem } from "./GifItem";
import { useFetchGifs } from "../hooks/useFetchGifs";

export const GifGrid = ({ category }) => {

  const {images, isLoading} = useFetchGifs(category)

  return (
    <div key="{category}">
      <h3>{category}</h3>
        {
          isLoading && (<p>Cargando información...</p>)
        }
      <div className="card-grid">

        {
          images.map( (image) => (
            <GifItem 
              key={image.id}
              {...image}
              />
          ))
        }
        </div>
    </div>
  );
};
